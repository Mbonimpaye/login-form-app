package com.Mbonimpaye.LOGIN;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class Registration extends Activity {
Button Gohome;
EditText Name;
Spinner courses;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registration);
		Name=(EditText)findViewById(R.id.name);
		
		//creating a Spinner
	   String[]courses=new String[]{"Computer Science","ICT","Law","Geophomatics","Education and ICT","Mathematics and Statistics"};
	 
		Spinner spinner=(Spinner)findViewById(R.id.courses);
		ArrayAdapter<String>adapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,courses);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);
		
		//creating a radiobutton
		
		
		Gohome=(Button)findViewById(R.id.button3);
		Gohome.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent nextIntent=new Intent(getApplicationContext(),Home.class);
				nextIntent.putExtra("Name", Name.getText().toString());
				
				
				
				startActivity(nextIntent);
				
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.registration, menu);
		return true;
	}

}
