package com.Mbonimpaye.LOGIN;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

public class Home extends Activity {
Button nextRegister;
String Name;
Spinner courses;
String Date;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		
		Intent Strings = getIntent();
		Name=Strings.getStringExtra("Name");
		TextView jina=(TextView)findViewById(R.id.name);
		jina.setText(Name);
		
		
		
		
		Strings=getIntent();
		Date=Strings.getStringExtra("Date");
		TextView tarehe=(TextView)findViewById(R.id.Date);
		tarehe.setText(Date);
		
		
		
		nextRegister=(Button)findViewById(R.id.button3);
		nextRegister.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent nextIntent=new Intent(getApplicationContext(),Registration.class);
				startActivity(nextIntent);
				
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home, menu);
		return true;
	}

}
